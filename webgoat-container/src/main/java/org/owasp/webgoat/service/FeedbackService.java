package org.owasp.webgoat.service;

import nz.ac.vuw.httpfuzz.jee.rt.*;
import org.owasp.webgoat.session.WebSession;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Service to pick up information gathered at runtime by means of instrumentation.
 * Logic copied from nz.ac.vuw.httpfuzz.jee.rt.TrackedInvocationsPickupServlet .
 * TODO: refactor -- remove redundant code
 * @author jens dietrich
 */
@RestController
public class FeedbackService {

    public static final String URL_FEEDBACK_MVC = Constants.FUZZING_FEEDBACK_PATH_TOKEN ; //"/service/feedback.mvc";
    private WebSession webSession = null;

    public FeedbackService(WebSession webSession) {
        this.webSession = webSession;
    }

    @GetMapping(path = URL_FEEDBACK_MVC+"/{id}")  // TODO: refactor content type
    @ResponseBody
    public ResponseEntity<String> getFeedback(@PathVariable("id") String id, @RequestParam(required = false) String applicationpackages) throws IOException  {

        if (id==null || id.length()==0) {
            throw new NotFoundException("No data found for id " + id);  // will generated 404
        }

        // request.getServletContext().log("TrackedInvocationsPickupServlet -- id exists: " + id);
        if (id.equals(Constants.SYSTEM_INVOCATIONS_TICKET)) {
            if (applicationpackages==null) {
                throw new ApplicationPackageParamMissingException("Request parameter applicationpackages is missing");
            }
            Set<String> invocations = SystemInvocationTracker.getTrackedInvocations(applicationpackages);

            StringWriter sw = new StringWriter();
            PrintWriter out = new PrintWriter(sw);
            for (String invocation:invocations) {
                out.println(invocation);
            }

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.TEXT_PLAIN);
            return  new ResponseEntity(sw.toString(),headers, HttpStatus.OK);
        }
        else {
            Map<DataKind, List<Object>> tracked = InvocationTracker.DEFAULT.pickup(id);
            if (tracked == null) {
                throw new NotFoundException("No data found for id " + id);  // will generated 404
            }

            StringWriter sw = new StringWriter();
            PrintWriter out = new PrintWriter(sw);
            Encoder.DEFAULT.encode(tracked, out);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType(Encoder.DEFAULT.getContentType()));
            return  new ResponseEntity(sw.toString(),headers, HttpStatus.OK);

        }

    }
}
